console.log("########## https://www.howtographql.com/graphql-js/1-getting-started/ ###########");
console.log("################################## ¯\\_(ツ)_/¯ ###################################");

import { GraphQLServer } from 'graphql-yoga';
import { Prisma } from 'prisma-binding';


import * as Query from './resolvers/Query';
import * as Mutation from './resolvers/Mutation';
import * as Subscription from './resolvers/Subscription';
import * as AuthPayload from './resolvers/AuthPayload';
import * as Feed from './resolvers/Feed';


const resolvers = {
  Query,
  Mutation,
  Subscription,
  AuthPayload,
  Feed
  /*Query: {
    info: () => 'myvids v1.0',
    feed: (root, args, context, info) => {
      return context. db.query.videos({}, info);
    }
  },*/

  /*Mutation: {
     post: (root, args, context, info) => {
      return context.db.mutation.createVideo({
        data: {
          url: args.url,
          title: args.title,
          description: args.description
        }
      }, info);
    } 
  }*/
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: (req) => ({
    ...req,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: 'http://localhost:4466',
      secret: 'mysecret123',
      debug: true,
    })
  })
})
server.start(() => console.log(`Server is running on http://localhost:4000`))
