export const videos = (parent, args, context, info) => {
  return context.db.query.videos({ where: { id_in: parent.videoIds } }, info)
}
