export const feed = async (root, args, context, info) => {

  const where = 
    args.filterString ? 
    {
      OR: [
        { title_contains: args.filterString },
        { description_contains: args.filterString },
        { url_contains: args.filterString }
      ]
    } : {};

  const queriedVideos = await context.db.query.videos(
    { 
      where, 
      skip: args.skip, 
      first: args.first,
      orderBy: args.orderBy 
    },
    `{id}`
  );

  const countSelectionSet = `
    {
      aggregate {
        count
      }
    }
  `;

  const videosConnection = await context.db.query.videosConnection({}, countSelectionSet)

  return {
    count: videosConnection.aggregate.count,
    videoIds: queriedVideos.map(video => video.id),
  }
  /*return context.db.query.videos(
    { 
      where: filter, 
      skip: args.skip, 
      first: args.first,
      orderBy: args.orderBy 
    }, 
    info);*/

}
