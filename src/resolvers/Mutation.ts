import { user } from './AuthPayload';

import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { APP_SECRET, getUserId } from '../utils';


export const post = function (root, args, context, info) {
  const userId = getUserId(context);
  return context.db.mutation.createVideo({
    data: {
      url: args.url,
      title: args.title,
      description: args.description,
      postedBy: { connect: { id: userId }}
    }
  }, info);
}

export const signup = async function (parent, args, context, info) {
  // 1
  const password = await bcrypt.hash(args.password, 10)
  // 2
  const user = await context.db.mutation.createUser({
    data: { ...args, password },
  }, `{ id }`)

  // 3
  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  // 4
  return {
    token,
    user,
  }
}

export const login = async function (parent, args, context, info) {
  // 1
  const user = await context.db.query.user({ where: { email: args.email } }, ` { id password } `)
  if (!user) {
    throw new Error('No such user found')
  }

  // 2
  const valid = await bcrypt.compare(args.password, user.password)
  if (!valid) {
    throw new Error('Invalid password')
  }

  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  // 3
  return {
    token,
    user,
  }
}

export const share =  async function (parent, args, context, info) {
  const userId = getUserId(context);
  
  const linkExists = await context.db.exists.Share({
    user: { id: userId },
    video: { id: args.videoId },
  })
  if (linkExists) {
    throw new Error(`Video: ${args.videoId} already shared!`)
  }

  return context.db.mutation.createShare(
    {
      data: {
        user: { connect: { id: userId } },
        video: { connect: { id: args.videoId } },
      },
    },
    info,
  )
}