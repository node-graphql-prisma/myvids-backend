
const newVideoSubscribe = (parent, args, context, info) => {
  return context.db.subscription.video(
    { where: { mutation_in: ['CREATED'] }},
    info
  )
}

export const newVideo = {
  subscribe: newVideoSubscribe
}

const newShareSubscribe = (parent, args, context, info) => {
  return context.db.subscription.share(
    { where: { mutation_in: ['CREATED'] }},
    info
  )
}

export const newShare = {
  subscribe: newShareSubscribe
}
